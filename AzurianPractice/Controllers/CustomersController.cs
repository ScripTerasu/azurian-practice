﻿using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using AzurianPractice.DataAccess;
using AzurianPractice.DataAccess.Entities;
using AzurianPractice.Helpers;
using Halcyon.WebApi.HAL;

namespace AzurianPractice.Controllers
{
    public class CustomersController : ApiController
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();

        // GET: api/Customers
        [Route("api/Customers", Name = "GetCustomers")]
        public IHttpActionResult GetCustomers(int page = 1, int pageSize = 10)
        {
            var skip = (page - 1) * pageSize;

            var total = _unitOfWork.CustomerRepository.GetTotal();
            var customers = _unitOfWork.CustomerRepository.Filter(null, skip, pageSize);
            var linkBuilder = new PageLinkBuilder(Url, "GetCustomers", null, page, pageSize, total);
            var pageLinks = linkBuilder.ToLinks();
            var resultModel = new
            {
                data = customers,
                count = customers.Count(),
                total = total
            };

            return this.HAL(
                resultModel, pageLinks
            );
            //return _unitOfWork.CustomerRepository.Get();
        }

        // GET: api/Customers/5
        [ResponseType(typeof(Customer))]
        public IHttpActionResult GetCustomer(int id)
        {
            var customer = _unitOfWork.CustomerRepository.GetById(id);
            if (customer == null) return NotFound();

            return Ok(customer);
        }

        // PUT: api/Customers/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCustomer(int id, Customer customer)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            if (id != customer.Id) return BadRequest();

            _unitOfWork.CustomerRepository.Update(customer);

            try
            {
                _unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CustomerExists(id)) return NotFound();

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Customers
        [ResponseType(typeof(Customer))]
        public IHttpActionResult PostCustomer(Customer customer)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            _unitOfWork.CustomerRepository.Insert(customer);
            _unitOfWork.Save();

            return CreatedAtRoute("DefaultApi", new {id = customer.Id}, customer);
        }

        // DELETE: api/Customers/5
        [ResponseType(typeof(Customer))]
        public IHttpActionResult DeleteCustomer(int id)
        {
            var customer = _unitOfWork.CustomerRepository.GetById(id);
            if (customer == null) return NotFound();

            _unitOfWork.CustomerRepository.Delete(customer);
            _unitOfWork.Save();

            return Ok(customer);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) _unitOfWork.Dispose();
            base.Dispose(disposing);
        }

        private bool CustomerExists(int id)
        {
            return _unitOfWork.CustomerRepository.Get(e => e.Id == id).Any();
        }
    }
}