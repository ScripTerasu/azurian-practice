﻿using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using AzurianPractice.DataAccess;
using AzurianPractice.DataAccess.Entities;
using AzurianPractice.Helpers;
using Halcyon.WebApi.HAL;

namespace AzurianPractice.Controllers
{
    public class ProductsController : ApiController
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();

        // GET: api/Products
        [Route("api/Products", Name = "GetProducts")]
        public IHttpActionResult GetProducts(int page = 1, int pageSize = 10)
        {
            var skip = (page - 1) * pageSize;

            var total = _unitOfWork.ProductRepository.GetTotal();
            var products = _unitOfWork.ProductRepository.Filter(null, skip, pageSize);
            var linkBuilder = new PageLinkBuilder(Url, "GetProducts", null, page, pageSize, total);
            var pageLinks = linkBuilder.ToLinks();
            var resultModel = new
            {
                data = products,
                count = products.Count(),
                total = total
            };
            
            return this.HAL(
                resultModel, pageLinks
            );
            //return _unitOfWork.ProductRepository.Get();
        }

        // GET: api/Products/5
        [ResponseType(typeof(Product))]
        public IHttpActionResult GetProduct(int id)
        {
            var product = _unitOfWork.ProductRepository.GetById(id);
            if (product == null) return NotFound();
            return Ok(product);
        }

        // PUT: api/Products/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutProduct(int id, Product product)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            if (id != product.Id) return BadRequest();

            _unitOfWork.ProductRepository.Update(product);

            try
            {
                _unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id)) return NotFound();

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Products
        [ResponseType(typeof(Product))]
        public IHttpActionResult PostProduct(Product product)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            _unitOfWork.ProductRepository.Insert(product);
            _unitOfWork.Save();

            return CreatedAtRoute("DefaultApi", new {id = product.Id}, product);
        }

        // DELETE: api/Products/5
        [ResponseType(typeof(Product))]
        public IHttpActionResult DeleteProduct(int id)
        {
            var product = _unitOfWork.ProductRepository.GetById(id);
            if (product == null) return NotFound();

            _unitOfWork.ProductRepository.Delete(product);
            _unitOfWork.Save();

            return Ok(product);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) _unitOfWork.Dispose();
            base.Dispose(disposing);
        }

        private bool ProductExists(int id)
        {
            return _unitOfWork.ProductRepository.Get(e => e.Id == id).Any();
        }
    }
}