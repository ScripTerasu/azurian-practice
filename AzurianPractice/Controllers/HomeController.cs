﻿using System.Web.Mvc;

namespace AzurianPractice.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Consume()
        {
            return View();
        }

        public ActionResult Api()
        {
            return View();
        }
    }
}