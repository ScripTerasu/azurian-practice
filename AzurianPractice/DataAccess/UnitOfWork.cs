﻿using System;
using AzurianPractice.DataAccess.Entities;

namespace AzurianPractice.DataAccess
{
    public class UnitOfWork : IDisposable
    {
        private readonly ExampleContext _context = new ExampleContext();
        private GenericRepository<Customer> _customerRepository;


        private bool _disposed;
        private GenericRepository<Product> _productRepository;


        public GenericRepository<Product> ProductRepository =>
            _productRepository ?? (_productRepository = new GenericRepository<Product>(_context));

        public GenericRepository<Customer> CustomerRepository =>
            _customerRepository ?? (_customerRepository = new GenericRepository<Customer>(_context));

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
                if (disposing)
                    _context.Dispose();
            _disposed = true;
        }
    }
}