﻿using System;
using System.Collections.Generic;
using System.Web.Http.Routing;
using Halcyon.HAL;

namespace AzurianPractice.Helpers
{
    public class PageLinkBuilder
    {
        public PageLinkBuilder(UrlHelper urlHelper, string routeName, object routeValues, int page, int pageSize,
            long totalRecordCount)
        {
            // Determine total number of pages
            var pageCount = totalRecordCount > 0
                ? (int) Math.Ceiling(totalRecordCount / (double) pageSize)
                : 0;

            // Create them page links 
            FirstPage = new Uri(urlHelper.Link(routeName, new HttpRouteValueDictionary(routeValues)
            {
                {"page", 1},
                {"pageSize", pageSize}
            }));
            LastPage = new Uri(urlHelper.Link(routeName, new HttpRouteValueDictionary(routeValues)
            {
                {"page", pageCount},
                {"pageSize", pageSize}
            }));
            if (page > 1)
                PreviousPage = new Uri(urlHelper.Link(routeName, new HttpRouteValueDictionary(routeValues)
                {
                    {"page", page - 1},
                    {"pageSize", pageSize}
                }));

            if (page < pageCount)
                NextPage = new Uri(urlHelper.Link(routeName, new HttpRouteValueDictionary(routeValues)
                {
                    {"page", page + 1},
                    {"pageSize", pageSize}
                }));
        }

        public Uri FirstPage { get; }
        public Uri LastPage { get; }
        public Uri NextPage { get; }
        public Uri PreviousPage { get; }

        public List<Link> ToLinks()
        {
            var links = new List<Link>();
            if (FirstPage != null)
                links.Add(new Link("first", FirstPage.ToString()));
            if (PreviousPage != null)
                links.Add(new Link("previous", PreviousPage.ToString()));
            if (NextPage != null)
                links.Add(new Link("next", NextPage.ToString()));
            if (LastPage != null)
                links.Add(new Link("last", LastPage.ToString()));
            return links;
        }
    }
}